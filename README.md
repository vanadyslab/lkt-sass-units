# Installation

Execute the next command:

```
npm i git+https://gitlab.com/Lekrat/lkt-sass-units.git
```

# Usage

## NPM

```
@import "~lkt-sass-units/src/lkt-sass-units.scss";
```

## Gulp

In your main.scss:

```
@import "./../path/to/../node_modules/lkt-sass-units/src/lkt-sass-units";
```

# Configure

After import in your project, you can customize:

```
$LKT_REM_FACTOR: 16;  // Factor to convert 1px to rem
$LKT_INCLUDE_PX: true; // Enable px property. If false, mixins will only generate converted properties
```

# Api

## Params
| Param | Description |
|---|---|
|$property|CSS Property|
|$value-in-px|Value in px|
|$parent-font-size-in-px|Parent font size in px. For em calculations|
|$targeted-viewport|Value in px of designed viewport or container size (only percentage)|
|$big-enough|Value in px to indicate at which targeted viewport must be a fixed value (max viewport size)|
|$small-enough|Value in px to indicate at which targeted viewport must be a fixed value (min viewport size)|

## Mixins

This package provides you the following mixins:

| Mixin | Description | Params |
|---|---|---|
| lkt-to-rem | Convert to rem | $property, $value-in-px, $factor |
| lkt-to-em | Convert to rem | $property, $value-in-px, $parent-font-size-in-px |
| lkt-to-vw | Convert to viewport width | $property, $value-in-px, $targeted-viewport, $big-enough, $small-enough |
| lkt-to-percent | Convert to container percentage | $property, $value-in-px, $targeted-viewport, $big-enough, $small-enough |
| lkt-to-rem-vw | Convert to rem and viewport width| $property, $value-in-px, $targeted-viewport, $big-enough, $small-enough |
| lkt-to-rem-percent | Convert to rem and percentage width| $property, $value-in-px, $targeted-viewport, $big-enough, $small-enough |
| lkt-to-rem-percent-vw | Convert to rem, percentage width and viewport width| $property, $value-in-px, $targeted-viewport, $big-enough, $small-enough |

## Functions

| Mixin | Description | Params |
|---|---|---|
| lkt-rem | Convert to rem | $value-in-px |
| lkt-em | Convert to em | $value-in-px, $parent-font-size-in-px |
| lkt-vw | Convert to viewport width | $value-in-px, $targeted-viewport |
| lkt-percent | Convert to container percentage | $value-in-px, $targeted-viewport |